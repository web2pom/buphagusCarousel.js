( function( window, undefined ) { 

	/**
	 * PLUGIN
	 */

	function buphagusCarousel (SELECTOR,OPTIONS) {

		var carousels  = [];
		var containers = "string" == typeof SELECTOR ? document.querySelectorAll(SELECTOR) : SELECTOR;

		var options = extend({
			autoplay   : 0,
			onInit     : false,
			onComplete : false,
			counter    : false,
			nav        : true,
			start      : 0
		}, OPTIONS);

		if ( 0 === containers.length ) return;

		for (var i = containers.length - 1; i >= 0; i--)
			carousels[i] = init(containers[i],options);

		return carousels;
	}

	/**
	 * EXTEND
	 */
	
	function extend() {
		// Variables
		var extended = {};
		var deep = false;
		var i = 0;
		var length = arguments.length;

		// Check if a deep merge
		if ( Object.prototype.toString.call( arguments[0] ) === '[object Boolean]' ) {
			deep = arguments[0];
			i++;
		}

		// Merge the object into the extended object
		var merge = function (obj) {
			for ( var prop in obj ) {
				if ( Object.prototype.hasOwnProperty.call( obj, prop ) ) {
					// If deep merge and property is an object, merge properties
					if ( deep && Object.prototype.toString.call(obj[prop]) === '[object Object]' ) {
						extended[prop] = extend( true, extended[prop], obj[prop] );
					} else {
						extended[prop] = obj[prop];
					}
				}
			}
		};

		// Loop through each object and conduct a merge
		for ( ; i < length; i++ ) {
			var obj = arguments[i];
			merge(obj);
		}

		return extended;
	}

	/**
	 * INIT
	 */

	function init(CONTAINER,OPTIONS) {

		// IF CALLBACK
		if( OPTIONS.onInit !== false ) 
			OPTIONS.onInit(CONTAINER,OPTIONS);

		// global
		var global = {
			options  : OPTIONS,
			elements : {
				container : CONTAINER,
				contents  : CONTAINER.childNodes,
				wrap      : document.createElement('div'),
				bar       : false,
				nav       : false,
				prev      : false,
				next      : false
			},
			data     : {
				total     : false,
				index     : OPTIONS.start,
				current   : false,
				old       : false,
				timeOutId : false
			}
		};

		/** Remove Empty Text */
		for (var i = global.elements.contents.length - 1; i >= 0; i--) {
			if(global.elements.contents[i].nodeType == 3 )
				global.elements.container.removeChild(global.elements.contents[i]);
		}

		/** Global */		
		if (!global.elements.container || 0 === global.elements.contents.length) return;
		
		if( "undefined" == typeof global.options.autoplay ) global.options.autoplay = 0;

		// PREPARE CLASSES //
		global.elements.container.classList.add('js', 'buphagusCarousel');

		// CREATE WRAP //
		global.elements.wrap.className = 'cowl-wrap';

		while( global.elements.contents.length > 0)
			if( global.elements.contents[0] !== undefined ) // IE8 FIX (dont know why)
				global.elements.wrap.appendChild(global.elements.contents[0]);

		global.elements.container.appendChild(global.elements.wrap);

		// PREPARE CONTENTS //

		global.elements.contents = global.elements.wrap.childNodes;
		global.data.total        = global.elements.contents.length;
		global.data.current      = global.elements.contents[global.data.index];

		global.data.current.classList.add('current');
		
		// CREATE BAR //
		if( true === global.options.counter || true === global.options.nav ){
			global.elements.bar           = document.createElement('div');
			global.elements.bar.className = 'cowl-bar';
		}

		if ( true === global.options.nav ) {
			// CREATE NAV //
			global.elements.nav  = document.createElement('div');
			global.elements.prev = document.createElement('div');
			global.elements.next = document.createElement('div');
			
			global.elements.prev.className = 'cowl-prev';
			global.elements.next.className = 'cowl-next';
			global.elements.nav.className  = 'cowl-nav';

			if (document.addEventListener){
				global.elements.next.addEventListener('click', goToNext);
				global.elements.prev.addEventListener('click', goToPrev);
			} else {
				global.elements.next.attachEvent('onclick', goToNext);
				global.elements.prev.attachEvent('onclick', goToPrev); 
			}
			
			global.elements.nav.appendChild(global.elements.prev);
			global.elements.nav.appendChild(global.elements.next);
			
			global.elements.bar.appendChild(global.elements.nav);

			global.elements.container.insertBefore(global.elements.bar, global.elements.wrap);
		}


		// IF COUNTER
		if( global.options.counter ) {
			global.elements.counter           = document.createElement('div');
			global.elements.counter.className = 'cowl-counter';

			update_counter();

			global.elements.bar.appendChild(global.elements.counter);
		}
		
		// IF AUTOPLAY
		if( global.options.autoplay > 0 ) autoplay();
		
		// IF CALLBACK
		if( global.options.onComplete !== false ) global.options.onComplete(global.elements, global.data, global.options);
			
		
		/************************************************
		 * FUNCTIONS
		 ************************************************/

		/**
		 * UPDATE_COUNTER
		 */
		
		function update_counter() {
			if( "undefined" == typeof global.elements.counter ) return;

			global.elements.counter.innerHTML = (global.data.index + 1) + ' / ' + global.data.total;
		}

		/**
		 * AUTOPLAY
		 */

		function autoplay(ON){
			ON = "undefined" == typeof ON ? true : ON;

			if( global.elements.autoplayID && ON ) return;

			if( "undefined" == typeof global.elements.autoplayID && ON ) {
				global.elements.autoplayID = setInterval(goToNext , global.options.autoplay);

				global.elements.container.onmouseover = function(){
					clearInterval(global.elements.autoplayID);
				};

				global.elements.container.onmouseout = function(){
					global.elements.autoplayID = setInterval(goToNext, global.options.autoplay);
				};
			} else global.elements.autoplayID = false;
		}

		/**
		 * goTo
		 */
		
		function goTo(index, direction) {
			
			// If index is not possible then return
			if( index >= global.data.total ) return;

			var animation = {
				enter : 'next',
				exit  : 'prev'
			};
			
			// Prepare Animation 
			if ( 'prev' == direction) {
				animation = {
					enter : 'prev',
					exit  : 'next'
				};
			}

			// PREPARE CLASSES //
			global.elements.container.classList.remove('js-animate');

			// // Reset Old //
			if(global.data.old){
				global.data.old.classList.remove(animation.exit);
				global.data.old.classList.add('inactive-' + animation.exit);
				// global.data.old.classList.remove(animation.enter);
				global.data.old.classList.remove('current');
			}

			global.data.index   = index;
			global.data.old     = global.data.current;
			global.data.current = global.elements.contents[global.data.index];

			// // SET ACTION //
			global.data.current.classList.remove('prev');
			global.data.current.classList.remove('inactive-' + 'prev');
			global.data.current.classList.remove('next');
			global.data.current.classList.remove('inactive-' + 'next');

			global.data.current.classList.add(animation.enter);
			window.clearTimeout(global.data.timeOutId);

			global.data.timeOutId = window.setTimeout(function(){
				global.elements.container.classList.add('js-animate');

				global.data.old.classList.remove('current');
				global.data.old.classList.add(animation.exit);
				
				global.data.current.classList.remove(animation.enter);
				global.data.current.classList.add('current');
			}, 50);
		}

		function goToNext() {
			goTo(global.data.index + 1 >= global.data.total ? 0 : global.data.index + 1 );
		}
		function goToPrev() {
			goTo(global.data.index - 1 < 0 ? global.data.total - 1 : global.data.index - 1, 'prev' );
		}

		/**
		 * debug
		 */
		
		function debug() {
			return global;
		}

		/**
		 * RETURN
		 */

		return {
			goTo  : goTo,
			debug : debug
		};
	}

	window.buphagusCarousel = buphagusCarousel;

} )( window );